// -----------------------------------------------------------
// ATtiny412 @ 8 MHz
// -----------------------------------------------------------
//                              -----\/-----
//                   VCC (B+)  1| VCC  GND |8  GND
// LED Strip FET (active low)  2| PA6  PA3 |7  LED Strip Data
//        Button (active low)  3| PA7  PA0 |6  UPDI (connected to USB D+)
//   Battery Measure (unused)  4| PA1  PA2 |5  N/C
//                              ------------

#define F_CPU 8000000UL

#define NUM_LEDS				26

#define BUTTON_PIN				PIN7_bm
#define LED_STRIP_POWER_PIN		PIN6_bm
#define LED_STRIP_DATA_PIN		PIN3_bm

#define LED_STRIP_PORT			VPORTA_OUT
#define LED_STRIP_PIN			3

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct pixelColor {
	uint8_t green;
	uint8_t red;
	uint8_t blue;
	uint8_t white;
} pixelColor;

typedef enum effects_t {
	RAINBOW_FAST,
	RAINBOW_SLOW,
	SPARKLE,
	FLAME,
	CHRISTMAS,
	RAVE,
	WAVE_WHITE,
	WAVE_RED,
	WAVE_BLUE,
	WAVE_GREEN,
	PULSE_WHITE,
	PULSE_RED,
	PULSE_BLUE,
	PULSE_GREEN,
	JUST_WHITE,
	JUST_RED,
	JUST_BLUE,
	JUST_GREEN,
	NUM_EFFECTS,
} effects_t;

volatile uint16_t timebase = 0;
uint8_t frameNum = 0;
effects_t activeEffect = 0;
pixelColor pixelData[NUM_LEDS] = { 0 };
bool reducedBrightess = false;

bool buttonPressed = false;
bool buttonDoublePressed = false;
bool buttonLongPressed = false;
bool ignoreButtonPress = false;

uint16_t lastButtonProcessed = 0;
uint16_t lastEffectUpdate = 0;

void __attribute__((noinline)) led_strip_write()
{
	uint8_t count = NUM_LEDS;
	
	pixelColor data[NUM_LEDS] = { 0 };
	pixelColor* dataPtr = data;
	
	memcpy(data, pixelData, sizeof(pixelData));
	
	// for reduced brightness, just clear every other pixel
	if (reducedBrightess) for (uint8_t i = 1; i < NUM_LEDS; i+=2) data[i] = (pixelColor){ 0, 0, 0, 0 };

	cli();   // disable interrupts to ensure timing
	while (count--)
	{
		// send color data to the RGBW LED strip
		asm volatile (
			"ld __tmp_reg__, %a0+\n"
			"rcall send_led_strip_byte%=\n"  // send green
			"ld __tmp_reg__, %a0+\n"
			"rcall send_led_strip_byte%=\n"  // send red
			"ld __tmp_reg__, %a0+\n"
			"rcall send_led_strip_byte%=\n"  // send blue
			"ld __tmp_reg__, %a0+\n"
			"rcall send_led_strip_byte%=\n"  // send white
			"rjmp led_strip_asm_end%=\n"     // jump past the assembly subroutines

			// send_led_strip_byte subroutine: sends a single color byte to the LED strip
			"send_led_strip_byte%=:\n"
			"rcall send_led_strip_bit%=\n"  // MSB (bit 7)
			"rcall send_led_strip_bit%=\n"
			"rcall send_led_strip_bit%=\n"
			"rcall send_led_strip_bit%=\n"
			"rcall send_led_strip_bit%=\n"
			"rcall send_led_strip_bit%=\n"
			"rcall send_led_strip_bit%=\n"
			"rcall send_led_strip_bit%=\n"  // LSB (bit 0)
			"ret\n"

			// send_led_strip_bit subroutine: Sends single bit to the LED strip by driving the data line
			// high for some time. The amount of time the line is high depends on whether the bit is 0 or 1,
			// but this function always takes the same time (2 us)
			"send_led_strip_bit%=:\n"
			"rol __tmp_reg__\n"							// rotate left through carry
			"sbi %[port], %[pin]\n"						// drive the line high
			"brcs .+2\n" "cbi %[port], %[pin]\n"		// if the bit to send is 0, drive the line low now
			"nop\n" "nop\n"								// delay
			"brcc .+2\n" "cbi %[port], %[pin]\n"		// if the bit to send is 1, drive the line low now
			"ret\n"

			"led_strip_asm_end%=: "
			: "=b" (dataPtr)
			: "0" (dataPtr),         // %a0 pointer to the next color to display
			  [port] "I" (_SFR_IO_ADDR(LED_STRIP_PORT)),
			  [pin]  "I" (LED_STRIP_PIN)
		);
	}
	sei();          // re-enable interrupts now that we are done
	_delay_us(80);  // send the reset signal.
}

void clearPixelData()
{
	memset(pixelData, 0, sizeof(pixelData));
}

void rotatePixelDataRight()
{
	pixelColor last;
	last = pixelData[NUM_LEDS - 1];
	for (int8_t j = NUM_LEDS - 1; j > 0; j--) pixelData[j] = pixelData[j - 1];
	pixelData[0] = last;
}

pixelColor colorWheel(uint8_t WheelPos)
{
	pixelColor newColor = { 0 };
		
	if (WheelPos < 85)
	{
		newColor.red = WheelPos * 3;
		newColor.green = 255 - WheelPos * 3;
		newColor.blue = 0;
		return newColor;
	}
	else if (WheelPos < 170) 
	{
		WheelPos -= 85;
		newColor.red = 255 - WheelPos * 3;
		newColor.green = 0;
		newColor.blue = WheelPos * 3;
		return newColor;
	}
	else
	{
		WheelPos -= 170;
		newColor.red = 0;
		newColor.green = WheelPos * 3;
		newColor.blue = 255 - WheelPos * 3;
		return newColor;
	}
}

void processButton()
{
	static int8_t debounceCount = 0;
	static bool debouncedState = false;
	static bool lastDebouncedState = false;
	static bool buttonProcessed = false;
	static uint8_t pressCount = 0;
	
	static uint16_t pressTime = 0;
	static uint16_t longPressTime = 0;
	
	// button debounce (active low)
	if ((PORTA_IN & BUTTON_PIN) == 0)
	{
		if (++debounceCount > 3)
		{
			debounceCount = 3;
			debouncedState = true;
		}
	}
	else
	{
		if (--debounceCount < -3)
		{
			debounceCount = -3;
			debouncedState = false;
		}
	}
	
	// button processing
	// button pressed
	if (lastDebouncedState == false && debouncedState == true)
	{
		longPressTime = timebase;
		if (pressCount == 0) pressTime = timebase;
		pressCount++;
	}
	// button held
	else if (lastDebouncedState == true && debouncedState == true)
	{
		if (!ignoreButtonPress && !buttonProcessed && timebase - longPressTime > 1250)	// ~2 sec
		{
			buttonLongPressed = true;
			buttonProcessed = true;
		}
	}
	// button released
	else if (lastDebouncedState == true && debouncedState == false)
	{
		if (timebase - pressTime > 125)
		{
			if (!ignoreButtonPress && !buttonProcessed)
			{
				if (pressCount == 1) buttonPressed = true;
				else if (pressCount == 2) buttonDoublePressed = true;
				buttonProcessed = false;
			}
			pressCount = 0;
			ignoreButtonPress = false;
		}
	}
	// button not pressed
	else if (lastDebouncedState == false && debouncedState == false)
	{
		if (timebase - pressTime > 200) pressCount = 0;
	}

	lastDebouncedState = debouncedState;
}

void goToSleep()
{
	clearPixelData();
	led_strip_write();
	PORTA_OUTSET = LED_STRIP_POWER_PIN;
	
	// wait for button to be released before going to sleep
	uint8_t buttonReleasedCount = 0;
	while (buttonReleasedCount < 10)
	{
		if ((PORTA_IN & BUTTON_PIN)) buttonReleasedCount++;
		else buttonReleasedCount = 0;
		_delay_ms(10);
	}
	
	PORTA_PIN7CTRL = PORT_ISC_BOTHEDGES_gc | PORT_PULLUPEN_bm;
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
	PORTA_INTFLAGS |= BUTTON_PIN;
	sei();
	sleep_cpu();
}

int main(void)
{
	CPU_CCP = CCP_IOREG_gc;				 // unlock
	CLKCTRL_MCLKCTRLB = CLKCTRL_PEN_bm;  // enable prescaler (div 2)
	
	PORTA_DIRSET = LED_STRIP_POWER_PIN | LED_STRIP_DATA_PIN;
	PORTA_DIRCLR = BUTTON_PIN;
	PORTA_OUTCLR = LED_STRIP_POWER_PIN | LED_STRIP_DATA_PIN;
	PORTA_PIN7CTRL = PORT_PULLUPEN_bm;

	TCA0_SINGLE_INTCTRL = TCA_SINGLE_OVF_bm;
	TCA0_SINGLE_PER = 4000;	 // 1ms
	TCA0_SINGLE_CTRLA = TCA_SINGLE_CLKSEL_DIV2_gc | TCA_SINGLE_ENABLE_bm;  // (8 mhz / 2) = 4 mhz
	sei();

	// if button is held at startup, ignore the press because it's probably still held from waking up from sleep
	if ((PORTA_IN & BUTTON_PIN) == 0) ignoreButtonPress = true;
	
	activeEffect = eeprom_read_byte(0);

    while (1) 
    {
		if (timebase - lastButtonProcessed > 2)
		{
			processButton();
			lastButtonProcessed = timebase;
		}
		
		if (buttonLongPressed)
		{
			buttonLongPressed = false;
			eeprom_write_byte(0, activeEffect);
			goToSleep();
		}
		else if (buttonDoublePressed)
		{
			buttonDoublePressed = false;
			reducedBrightess = !reducedBrightess;
			frameNum = 0;
			clearPixelData();
		}
		else if (buttonPressed)
		{
			buttonPressed = false;
			if (++activeEffect >= NUM_EFFECTS) activeEffect = 0;
			frameNum = 0;
			lastEffectUpdate = 0;
			clearPixelData();
		}

		switch (activeEffect)
		{
			case JUST_WHITE:
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].white = 127;
					led_strip_write();
					frameNum++;
				}
			}
			break;
			
			case JUST_RED:
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].red = 127;
					led_strip_write();
					frameNum++;
				}
			}
			break;
			
			case JUST_BLUE:
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].blue = 127;
					led_strip_write();
					frameNum++;
				}
			}
			break;
			
			case JUST_GREEN:
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].green = 127;
					led_strip_write();
					frameNum++;
				}
			}
			break;
			
			case RAINBOW_FAST:
			{
				if (timebase - lastEffectUpdate > 30)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i] = colorWheel(((i * 256 / NUM_LEDS) + frameNum) & 255);
					led_strip_write();
					frameNum += 10;
					lastEffectUpdate = timebase;
				}
			}
			break;
			
			case RAINBOW_SLOW:
			{
				if (timebase - lastEffectUpdate > 20)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i] = colorWheel(((i * 256 / NUM_LEDS) + frameNum) & 255);
					led_strip_write();
					frameNum += 2;
					lastEffectUpdate = timebase;
				}
			}
			break;
			
			case CHRISTMAS:
			{
				if (timebase - lastEffectUpdate > 150)
				{
					if (frameNum == 0)
					{
						pixelData[0].red = 255;
						pixelData[1].red = 255;
						pixelData[2].red = 255;
						pixelData[3].red = 255;
						pixelData[4].red = 255;
						pixelData[5].red = 255;
						
						pixelData[6].green = 255;
						pixelData[7].green = 255;
						pixelData[8].green = 255;
						pixelData[9].green = 255;
						pixelData[10].green = 255;
						pixelData[11].green = 255;
						pixelData[12].green = 255;
						
						pixelData[13].red = 255;
						pixelData[14].red = 255;
						pixelData[15].red = 255;
						pixelData[16].red = 255;
						pixelData[17].red = 255;
						pixelData[18].red = 255;
						pixelData[19].red = 255;
						
						pixelData[20].green = 255;
						pixelData[21].green = 255;
						pixelData[22].green = 255;
						pixelData[23].green = 255;
						pixelData[24].green = 255;
						pixelData[25].green = 255;
						
						frameNum++;
					}
					else rotatePixelDataRight();

					led_strip_write();
					lastEffectUpdate = timebase;
				}
			}
			break;
			
			case SPARKLE:
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].white = 0x10;
					pixelData[rand() % NUM_LEDS + 1].white = 0xFF;
					pixelData[rand() % NUM_LEDS + 1].white = 0xFF;
					pixelData[rand() % NUM_LEDS + 1].white = 0xFF;
					led_strip_write();
					frameNum++;
					lastEffectUpdate = timebase;
				}
				else if (frameNum == 1)
				{
					if (timebase - lastEffectUpdate > 50)
					{
						for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].white = 0x10;
						led_strip_write();
						frameNum++;
						lastEffectUpdate = timebase - (rand() % 250);
					}
				}
				else
				{
					if (timebase - lastEffectUpdate > 500)
					{
						frameNum = 0;
					}
				}
			}
			break;
			
			case WAVE_WHITE:
			{
				if (timebase - lastEffectUpdate > 75)
				{
					if (frameNum == 0)
					{
						pixelData[0].white = 0;
						pixelData[1].white = 2;
						pixelData[2].white = 4;
						pixelData[3].white = 9;
						pixelData[4].white = 15;
						pixelData[5].white = 24;
						pixelData[6].white = 36;
						pixelData[7].white = 51;
						pixelData[8].white = 70;
						pixelData[9].white = 93;
						pixelData[10].white = 121;
						pixelData[11].white = 154;
						pixelData[12].white = 255;

						pixelData[13].white = 0;
						pixelData[14].white = 2;
						pixelData[15].white = 4;
						pixelData[16].white = 9;
						pixelData[17].white = 15;
						pixelData[18].white = 24;
						pixelData[19].white = 36;
						pixelData[20].white = 51;
						pixelData[21].white = 70;
						pixelData[22].white = 93;
						pixelData[23].white = 121;
						pixelData[24].white = 154;
						pixelData[25].white = 255;

						frameNum++;
					}
					else
					{
						rotatePixelDataRight();
						if (reducedBrightess) rotatePixelDataRight();
					}

					led_strip_write();
					lastEffectUpdate = timebase;
				}
			}
			break;

			case WAVE_RED:
			{
				if (timebase - lastEffectUpdate > 75)
				{
					if (frameNum == 0)
					{
						pixelData[0].red = 0;
						pixelData[1].red = 2;
						pixelData[2].red = 4;
						pixelData[3].red = 9;
						pixelData[4].red = 15;
						pixelData[5].red = 24;
						pixelData[6].red = 36;
						pixelData[7].red = 51;
						pixelData[8].red = 70;
						pixelData[9].red = 93;
						pixelData[10].red = 121;
						pixelData[11].red = 154;
						pixelData[12].red = 255;

						pixelData[13].red = 0;
						pixelData[14].red = 2;
						pixelData[15].red = 4;
						pixelData[16].red = 9;
						pixelData[17].red = 15;
						pixelData[18].red = 24;
						pixelData[19].red = 36;
						pixelData[20].red = 51;
						pixelData[21].red = 70;
						pixelData[22].red = 93;
						pixelData[23].red = 121;
						pixelData[24].red = 154;
						pixelData[25].red = 255;
						
						frameNum++;
					}
					else
					{
						rotatePixelDataRight();
						if (reducedBrightess) rotatePixelDataRight();
					}
					
					led_strip_write();
					lastEffectUpdate = timebase;
				}
			}
			break;
		
			case WAVE_BLUE:
			{
				if (timebase - lastEffectUpdate > 75)
				{
					if (frameNum == 0)
					{
						pixelData[0].blue = 0;
						pixelData[1].blue = 2;
						pixelData[2].blue = 4;
						pixelData[3].blue = 9;
						pixelData[4].blue = 15;
						pixelData[5].blue = 24;
						pixelData[6].blue = 36;
						pixelData[7].blue = 51;
						pixelData[8].blue = 70;
						pixelData[9].blue = 93;
						pixelData[10].blue = 121;
						pixelData[11].blue = 154;
						pixelData[12].blue = 255;

						pixelData[13].blue = 0;
						pixelData[14].blue = 2;
						pixelData[15].blue = 4;
						pixelData[16].blue = 9;
						pixelData[17].blue = 15;
						pixelData[18].blue = 24;
						pixelData[19].blue = 36;
						pixelData[20].blue = 51;
						pixelData[21].blue = 70;
						pixelData[22].blue = 93;
						pixelData[23].blue = 121;
						pixelData[24].blue = 154;
						pixelData[25].blue = 255;
					
						frameNum++;
					}
					else
					{
						rotatePixelDataRight();
						if (reducedBrightess) rotatePixelDataRight();
					}
				
					led_strip_write();
					lastEffectUpdate = timebase;
				}
			}
			break;
			
			case WAVE_GREEN:
			{
				if (timebase - lastEffectUpdate > 75)
				{
					if (frameNum == 0)
					{
						pixelData[0].green = 0;
						pixelData[1].green = 2;
						pixelData[2].green = 4;
						pixelData[3].green = 9;
						pixelData[4].green = 15;
						pixelData[5].green = 24;
						pixelData[6].green = 36;
						pixelData[7].green = 51;
						pixelData[8].green = 70;
						pixelData[9].green = 93;
						pixelData[10].green = 121;
						pixelData[11].green = 154;
						pixelData[12].green = 255;

						pixelData[13].green = 0;
						pixelData[14].green = 2;
						pixelData[15].green = 4;
						pixelData[16].green = 9;
						pixelData[17].green = 15;
						pixelData[18].green = 24;
						pixelData[19].green = 36;
						pixelData[20].green = 51;
						pixelData[21].green = 70;
						pixelData[22].green = 93;
						pixelData[23].green = 121;
						pixelData[24].green = 154;
						pixelData[25].green = 255;

						frameNum++;
					}
					else
					{
						rotatePixelDataRight();
						if (reducedBrightess) rotatePixelDataRight();
					}

					led_strip_write();
					lastEffectUpdate = timebase;
				}
			}
			break;
			
			case RAVE:
			{
				if (timebase - lastEffectUpdate > 20)
				{
					if (frameNum <= 1) 
					{
						clearPixelData();
						pixelColor randColor = colorWheel(rand() % 255);
						for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i] = randColor;
						led_strip_write();
					}
					else if (frameNum < 17)	
					{
						for (uint8_t i = 0; i < NUM_LEDS; i++) 
						{
							if (pixelData[i].red > 15) pixelData[i].red -= 15;
							if (pixelData[i].green > 15) pixelData[i].green -= 15;
							if (pixelData[i].blue > 15) pixelData[i].blue -= 15;
						}
						led_strip_write();
					}
					else if (frameNum < 25) { /* do nothing! delay */ }
					else frameNum = 0;
				
					frameNum++;
					lastEffectUpdate = timebase;
				}
			}
			break;
			
			case FLAME:
			{
				if (timebase - lastEffectUpdate > 100)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++)
					{
						uint8_t flicker = rand() % 55;
						pixelData[i].red = 250 - flicker;
						pixelData[i].green = 80 - flicker;
					}
					led_strip_write();
					lastEffectUpdate = timebase - (rand() % 100);
				}
			}
			break;
			
			case PULSE_WHITE:
				if (timebase - lastEffectUpdate > 20)
				{
					if (frameNum == 0) 
					{
						for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].white = 15;
						led_strip_write();
					}
					else if (frameNum < 49) 
					{ 
						for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].white += 5;
						led_strip_write();
					}
					else if (frameNum < 59) { /* do nothing! delay */ }
					else if (frameNum < 107) 
					{ 
						for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].white -= 5;
						led_strip_write();
					}
					else if (frameNum < 117) { /* do nothing! delay */ }
					else frameNum = 0;
					
					frameNum++;
					lastEffectUpdate = timebase;	
				}
			break;

			case PULSE_RED:
			if (timebase - lastEffectUpdate > 20)
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].red = 15;
					led_strip_write();
				}
				else if (frameNum < 49)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].red += 5;
					led_strip_write();
				}
				else if (frameNum < 59) { /* do nothing! delay */ }
				else if (frameNum < 107)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].red -= 5;
					led_strip_write();
				}
				else if (frameNum < 117) { /* do nothing! delay */ }
				else frameNum = 0;
				
				frameNum++;
				lastEffectUpdate = timebase;
			}
			break;
			
			case PULSE_BLUE:
			if (timebase - lastEffectUpdate > 20)
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].blue = 15;
					led_strip_write();
				}
				else if (frameNum < 49)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].blue += 5;
					led_strip_write();
				}
				else if (frameNum < 59) { /* do nothing! delay */ }
				else if (frameNum < 107)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].blue -= 5;
					led_strip_write();
				}
				else if (frameNum < 117) { /* do nothing! delay */ }
				else frameNum = 0;
							
				frameNum++;
				lastEffectUpdate = timebase;
			}
			break;
						
			case PULSE_GREEN:
			if (timebase - lastEffectUpdate > 20)
			{
				if (frameNum == 0)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].green = 15;
					led_strip_write();
				}
				else if (frameNum < 49)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].green += 5;
					led_strip_write();
				}
				else if (frameNum < 59) { /* do nothing! delay */ }
				else if (frameNum < 107)
				{
					for (uint8_t i = 0; i < NUM_LEDS; i++) pixelData[i].green -= 5;
					led_strip_write();
				}
				else if (frameNum < 117) { /* do nothing! delay */ }
				else frameNum = 0;
										
				frameNum++;
				lastEffectUpdate = timebase;
			}
			break;
			
			default:
				activeEffect = 0;
			break;
		}
	}
}

ISR(TCA0_OVF_vect)
{
	timebase++;
	TCA0_SINGLE_INTFLAGS = TCA_SINGLE_OVF_bm;
}

ISR(PORTA_PORT_vect)
{
	// waking back up...
	cli();
	if (PORTA_INTFLAGS & BUTTON_PIN)
	{
		_delay_ms(10);
		bool shouldWake = true;
		
		// check if button is continued held for ~2 sec
		for (uint8_t x = 0; x < 15; x++)
		{
			if (PORTA_IN & BUTTON_PIN) { shouldWake = false; break; }
			_delay_ms(100);
		}
		
		if (shouldWake)
		{
			CPU_CCP = CCP_IOREG_gc;				// unlock
			RSTCTRL_SWRR = RSTCTRL_SWRE_bm;		// sw reset
		}
	}
	
	// go back to sleep
	PORTA_INTFLAGS |= BUTTON_PIN;
	sei();
	sleep_cpu();
}